INTRODUCTION
------------

Select Display Field Collection is a module that display the content of a Field Collection filtered by a select.


INSTALLATION
------------

 * Install as usual, see http://drupal.org/node/70151 for further information.


CONFIGURATION
-------------
 1. Go to the Manage Display page for any Entity that contains a
    field-collection field.

 2. Change the formatter to Select Display

 3. Click the gear icon on the right to access the formatter settings.

 4. (REQUIRED) Select the field to use for the select values.

 5. (REQUIRED) Select the field to use for the select contents.

 6. Optionally tick the box to strip all HTML tags from the select content.


CONTACT
-------

Current maintainers:
* Guilherme Vendramini (Vendramini) - https://www.drupal.org/u/vendramini
