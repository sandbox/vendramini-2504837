(function ($) {
	Drupal.behaviors.select_display_field_collection = {
		attach: function(context) {
		    var content = $(".select-display-field-collection-content");
		    var select = $(".select-display-field-collection-select select");

		    select.change(function(){
		    	content.children().hide();
		    	content.show();
		        $(this).find("option:selected").each(function(){
		            content.children().eq($(this).val()).show();
		        });
		    }).change();
		}
	}
})(jQuery);