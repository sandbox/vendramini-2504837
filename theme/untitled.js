jQuery(document).ready(function(){
    var content = jQuery(".select-display-field-collection-content");
    var select = jQuery(".select-display-field-collection-select select");

    content.children().hide();
    select.change(function(){
        jQuery(this).find("option:selected").each(function(){
            console.log(jQuery(this).val());
            content.find(".field--name-field-corpo-select div").eq(jQuery(this).val()).show();
        });
    }).change();
});